# contao-rms-bundle (Release Management System)

## Allgemeine Beschreibung
contao-rms-bundle ist ein Contao-Symfony-Bundle, welches es Benutzern einer bestimmten Benutzergruppe erlaubt, Freigaben zu verwalten. 

Die Freigaben sind im Moment für die Bereiche 

- Artikel, 
- alle Inhaltselemente egal unter welchen Elterntabelle, 
- News, 
- Newsletter, 
- FAQ
- Calender-Events 

umgesetzt worden. 

Weitere Erläuterungen finden sie weiter unten unter "Funktionsweise und Möglichkeiten"

## System-Vorraussetzungen
- \>= Contao 4.9.*
- \>= PHP 7.2

## Installation
1. Entweder per `composer require srhinow/contao-rms-bundle` oder per /contao-manager.phar.php nach "srhinow/contao-rms-bundle" in den Paketen suchen und installieren.
2. Die Datenbank per z.B. contao/install oder auf der Komandozeile mit `/vendor/bin/contao-console contao:migrade` aktualisieren.

## Konfiguration
[Anleitung wie Sie das Freigabe-Modul in Contao einrichten](docs/Configuration.md)

## Funktionsweise und Möglichkeiten
### Funktionsweise für nicht Freigabe-berechtige Redakteure
Alle Redakteur-Zugänge, die nicht Freigabe-berechtigt sind, können zwar Änderungen an Inhalten vornehmen, oder wenn es ihnen erlaubt ist, auch neue Inhalte erstellen. Diese werden aber nach dem Speichern **nicht Live** angezeigt, sondern stehen in einer Freigabeliste (Contao-Backend-Modul) dem Freigabe-berechtigten Backend-Benutzern zur Ansicht und bearbeitung bereit. 

Damit der jeweils zugewiesene Freigabe-Redakteur nicht nach jedem speichern einer Änderung eine E-Mail bekommt, hat der "normale" Redakteur die Möglichkeit in der Bearbeiten-Ansicht eine Freigabe-Notiz zu hinterlassen und eine checkbox "Änderungsbescheid senden" um die Nachricht an den Freigabe-Redakteur zusenden. 

![Screenshot Redaktion][content_edit_rms_fields]

Außerdem sind Inhalte die auf Freigabe warten, in der Listenansicht im jeweiligen Bereich mit einem roten Hinweis (Änderungen ohne Freigabe) gekennzeichnet. So ist auch für andere Redakteure ersichtlich, das diese Einträge nicht bearbeitet werden sollten, solange dieser Hinweis dort steht. 

Mit Klick auf das Globus-Icon kann man sich die Änderungen auf der Website anzeigen lassen. 

![Screenshot Redaktion][content_list_changes]

### Funktionsweise für Freigabe-berechtige Backend-Benutzer
In dem BE-Modul 'Freigabe-Anfragen' hat der jeweils zugewiesene Freigabe-Redakteur folgende Möglichkeit:

![Screenshot Redaktion][rms_menu]
![Screenshot Redaktion][aenderungs_liste]

 1. die Änderungen können in einer Frontendvorschau angezeigt werden ![Screenshot Redaktion][aenderung_frontend_link]
 2. Man kann den Inhalt direkt editieren. ![Screenshot Redaktion][icon_edit]
 3. per Checkbox wiederum den Redakteur über die Reaktion per E-Mail und per Freigabe-Notizfeld zu informieren.
 ![Screenshot Redaktion][freigabe_notiz] 
 4. Sobald eine Anfrage im Bearbeiten-Modus gespeichert wurde, erscheint diese Anfrage in der Liste (grün) als bearbeitet markiert. 
 5. Zur besseren Übersicht kann die Freigabeliste auch nach verschiedenen Sachen wie z.B. dem Bearbeiten-Status gefiltert werden. ![Screenshot Redaktion][freigabe_liste_filter]
 6. Sobald eine Freigabe-Anfrage mit dem Häkchen  ![Screenshot Redaktion][icon_check] in der Freigabe-Liste bestätigt wird, wird der Inhalt **Live** gestellt und der Eintrag aus der Freigabeliste gelöscht.
 7. Wenn der Freigabe-Eintrag mit dem roten Kreuz ![Screenshot Redaktion][icon_delete] gelöscht wird, wird in bestehenden Elementen der Freigabemodus zurückgesetzt, neu angelegte Elemente gelöscht und der Eintrag aus der Freigabeliste entfernt.
 8. Mit einem Klick auf  ![Screenshot Redaktion][icon_version] erscheint eine Unterschiede-Ansicht. Die roten Bereiche sind die orginalen wie sie aktuell für jeden auf der Website zusehen sind und die grünen Bereiche sind die Änderungen welche zur Freigabe bereitstehen.  ![Screenshot Redaktion][aenderung_version_vergleich]


## finanziert durch

Hier ist [eine Aufstellung aller die diese Erweiterung finaziell unterstützt haben](docs/Sponsors.md)


## Entwickler-Informationen
[Hier sind ein paar Hinweise für Entwickler wie sie sich in die Freigabe-Logik einklinken können](docs/DevInfos.md)

---
[content_list_changes]: docs/images/inhalte_liste_bearbeitet.png "bearbeitete Inhalte in der Listenansicht" 
[content_edit_rms_fields]: docs/images/inhalte_redakteur_freigabe_einstellungen.png "bearbeitete Inhalte zur Pruefung senden" 
[rms_menu]: docs/images/contao_backend_menu_rms.png "Contao Menü RMS" 
[aenderungs_liste]: docs/images/aenderungs_liste.png "Liste der Änderungen" 
[aenderung_version_vergleich]: docs/images/aenderung_versionierung_anzeige.png "Änderungen in der Versions-Ansicht anzeigen" 
[aenderung_frontend_link]: docs/images/aenderung_frontend_vorschau.png "Änderungen im Frontend anzeigen" 
[icon_edit]: docs/images/icon_edit.png "Änderungen bearbeiten" 
[icon_version]: docs/images/icon_version.png "Änderungen vergleichen" 
[icon_check]: docs/images/icon_bestaetigen.png "Änderungen bestaetigen" 
[icon_delete]: docs/images/icon_loeschen.png "Änderungen loeschen" 
[freigabe_liste_filter]: docs/images/freigaben_liste_filteroptionen.png "Liste per Filter einschränken" 
[freigabe_notiz]: docs/images/korrekturnotiz_an_redakteur.png "Notiz an Redakteur senden" 