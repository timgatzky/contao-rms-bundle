# Konfiguration
Hier wird beschrieben wie Sie die Freigabe-Erweiterung contao-rms-bundle einrichten.

## Freigabemodul aktivieren

Um das Freigabemodul nach der Installation zu aktivieren, gehen Sie im Backend unter System/Einstellungen -> Abschnitt "Freigabe-Modul" und 
 setzen den Haken vor "Freigabe-Modus aktivieren" und speichern Sie die Einstellungen. 
 
 ![Screenshot Backend Einstellungen][rms_active]
 
 Danach sollte Backend im Bereich "Inhalte" der Punkt "Freigabe-Anfragen" zusehen sein.

![Screenshot Backend Einstellungen][rms_menu]

## Benutzer / Benutzergruppen
Grundsätzlich sind alle als Admin markierten Backend-Benutzer natürlich auch berechtigt Freigaben zu verwalten. Sollten aber Benutzer mit eingeschränkte Benutzerrechten diese Freigaben verwalten dürfen, muss dies in einer eigenen Benutzergruppe geschehen. Sollte dies der Fall sein legen sie hierzu wie folgt beschrieben eine eigene Benutzergruppe an.
 
Legen Sie im Bereich Benutzerverwaltung/ Benutzergruppen eine Gruppe an die als Freigabe-berechtigte Gruppe dienen soll. Danach weisen Sie mindestens einem Benutzer diese Gruppe zu.

***TIPP: man sucht in den Berechtigungseinstellungen mit Hilfe der Browser-Suche nach "Freigabe" und arbeitet sich so durch die einzelnen Bereiche.***

![Screenshot Backend Einstellungen][user_group_settings_1]

Soll der RMS-Redakteur auch die Möglichkeit haben, die Rms-Einstellungen zu setzen, geben sie dieser Gruppe auch folgende Rechte:

![Screenshot Backend Einstellungen][user_group_settings_2]

## zentrale Freigabe-Einstellungen
3. Wechseln Sie nun in das Backend-Modul "Freigabe-Anfragen" / "Freigabe-Einstellungen" 
Sie haben hier die Möglichkeit grundlegende Einstellung festzulegen. 

Name der Einstellung | Pflicht | Beschreibung
--- | --- | ---
Gruppe mit Freigabeberechtigung | ja | Legen sie hier fest welche Gruppe die Berechtigung haben soll Inhaltsänderungen zu bestätigen oder abzulehnen
Fallback-RMS-Redakteur | ja | Weisen sie in dem Selectfeld einen Freigabe-redakteur zu der im Falle das in einem Bereich ein Freigabe-Ansprechpartner fehlt diese Freigabeanfragen bekommt.
 zusätzliche Empfänger-E-Mail-Adressen | optional | z.B. für Urlaubsvertretungen oder Krankheiten können hier kommagetrennt ein oder mehrere E-Mail-Adressen eingetragen werden.
zu ignorierende Inhaltselemente | optional | Das bezieht sich hauptsächlich auf die tl_content. Das hier z.B. Inhaltselemente die z.B. für Design und nicht direkt für freigabe-kritischen Inhalt verantwortlich sind von der Freigabe-Prozedur ausgeschlossen sind. Hierzu muss kommagetrennt die in der Datenbank bekannten Spaltennamen haben.
zu ignorierende Felder in der Vergleichsansicht | optional | Für die Versions-Vergleichsansicht können hier Spalten ignoriert werden z.B. das tstamp oder modify-Feld wäre so ein typisches Beispiel.

## Freigabe-Einstellungen der jeweiligen Bereiche
Die Zuweisung der Redakteure findet immer in den Root-Einstellungen des jeweiligen Bereiches statt. So können verschiedene Bereiche verschiedenen Verantwortlichen zugewiesen werden. Der zugewiesene Freigabe-Redakteur sieht dann unter der Freigabe-Verwaltung auch nur die Freigaben, welche für ihn bestimmt sind.

### Artikel und Inhaltselemente
Für Artikel und Inhaltselemente eines bestimmten Seitenstrukturbaumes werden die Einstellungen in dem Seitenstruktur/Startpunkt einer Seite festgelegt.

### News
Für die Inhalte und Änderungen der News finden Sie die Freigabe-Einstellungen in den News-Archiv-Einstellungen

Damit die Vorschau für die News im Frontend angezeigt werden kann, muss dafür:
- ein Frontend-Modul **newsreaderrms** angelegt werden. Dies ist eine Kopie des originalen newsreader-Moduls welches im compile-Bereich eine zusätzliche Abfrage hat (do=preview und ob nen rms-Eintrag für diese Anzeige vorliegt). Wenn das der Fall ist, wird das Datenbankobject temp. für die Ausgabe überschrieben und weiter wie im originalen News-Reader-Modul abgearbeitet.
- eine Kopie der News-Reader-Seite angelegt mit den Einstellungen das es per robots nicht indexiert wird und das die Seite nicht im Suchindex auftaucht. (solange der Paramter do=preview nicht mit übergeben wird wird der aktuelle live-Datensatz angezeigt). Dort muss dann statt dem originalen newsreader-Modul das mit der RMS-Preview eingestellt werden.
- In den Einstellungen des jeweiligen Nachrichten-Archives muss dann diese Nachrichten-Details-RMS-Preview Seite eingestellt werden.

### Events / Veranstaltungen
Für die Inhalte und Änderungen der Events finden Sie die Freigabe-Einstellungen in den Kalender-Einstellungen

Damit die Vorschau für die Calendar-Events im Frontend angezeigt werden kann, muss dafür:
- ein Frontend-Modul **eventreaderrms** angelegt werden. Dies ist eine Kopie des originalen eventreader-Moduls welches im compile-Bereich eine zusätzliche Abfrage hat (do=preview und ob nen rms-Eintrag für diese Anzeige vorliegt). Wenn das der Fall ist, wird das Datenbankobject temp. für die Ausgabe überschrieben und weiter wie im originalen Event-Reader-Modul abgearbeitet.
- eine Kopie der Event-Reader-Seite angelegt mit den Einstellungen das es per robots nicht indexiert wird und das die Seite nicht im Suchindex auftaucht. (solange der Paramter do=preview nicht mit übergeben wird wird der aktuelle live-Datensatz angezeigt)
- In den Einstellungen des jeweiligen Kalenders muss dann diese Event-Details-RMS-Preview Seite eingestellt werden.

### Newsletter
Für die Inhalte und Änderungen der Newsletter finden Sie die Freigabe-Einstellungen in den Verteiler-Einstellungen

Damit die Vorschau für die Calendar-Events im Frontend angezeigt werden kann, muss dafür:
- ein Frontend-Modul **newsletterreaderrms** angelegt werden. Dies ist eine Kopie des originalen newsletterreader-Moduls welches im compile-Bereich eine zusätzliche Abfrage hat (do=preview und ob nen rms-Eintrag für diese Anzeige vorliegt). Wenn das der Fall ist, wird das Datenbankobject temp. für die Ausgabe überschrieben und weiter wie im originalen Newseletter-Reader-Modul abgearbeitet.
- eine Kopie der Newseletter-Reader-Seite angelegt mit den Einstellungen das es per robots nicht indexiert wird und das die Seite nicht im Suchindex auftaucht. (solange der Paramter do=preview nicht mit übergeben wird wird der aktuelle live-Datensatz angezeigt)
- In den Einstellungen des jeweiligen Verteilers muss dann diese Newseletter-Details-RMS-Preview Seite eingestellt werden.

### FAQ
Für die Änderungen der FAQ-Einträge finden Sie die Freigabe-Einstellungen in der jeweilig übergeordneten FAQ-Kategorie

**Für FAQ's ist aktuell noch keine Preview-Version umgesetzt**

### Anmerkung für eigene / weitere Module
Sollen die Freigabe-Mechanismen auch für eigene  oder andere Module greifen, können Freigaben genauso wie beschrieben aufgebaut werden. Da ich es so flexibel strukturiert habe das er sich immer die oberste Ebene ermittelt (per ptable) und dort nach den Einstellungen sucht. Das heißt das es dort nach den Freigabe-Einstellungs-feldern ('rms_protected','rms_master_member','rms_preview_jumpTo') sucht. Wenn diese gesetzt wurden müssen in den Inhaltssensiblen Modulbereichen folgende Felder für die Freigabe-Verwaltung erstellt werden ('rms_first_save','rms_new_edit','rms_ref_table','rms_notice','rms_release_info'). Bitte dann diese dca-Einstellungen vom contao-rms-module abschauen und übernehmen.

---

[rms_active]: images/contao_backend_rms_aktivieren.png "Contao Backend Einstellungen" 
[rms_menu]: images/contao_backend_menu_rms.png "Contao Menü RMS" 
[user_group_settings_1]: images/user_group_settings_allow_module.png "Modul-Eintrag sichtbar machen" 
[user_group_settings_2]: images/user_group_settings_tl_rms_settings.png "RMS-Settings erlauben" 

