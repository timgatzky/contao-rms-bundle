# finanziert durch

Die erste Umsetzung der rms-Erweiterung für Contao 2.11.x ist durch Anforderungen in einem Auftrag und mit der finanziellen Unterstützung der **Büchereizentrale Niedersachsen** (http://www.bz-niedersachsen.de) entstanden.

Die Umsetzung der rms-Erweiterung für Contao 3 und die Erweiterung um eine "Unterschiede anzeigen" - Funktion, wurde durch **Cyrill Weiss** (http://www.cyrillweiss.ch/) in Auftrag gegeben und finanziert.

Die Umarbeitung der rms-Erweiterung, so das zukünftig pro Bereich ein eigener Freigabe-Redakteur zugewiesen werden kann, wurde durch **Michael Roedhamer** (http://pixelkinder.com) in Auftrag gegeben und finanziert.

Die Migration zu einem Contao-Bundle für Contao 4.9.* hat die **Agentur AD Konzept GmbH** aus Leipzig in Auftrag gegeben.

