<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Hook;

use Contao\ContentElement;
use Contao\ContentModel;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Input;
use Contao\StringUtil;
use Psr\Log\LogLevel;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

class GetContentElementListener
{
    public function onGetContentElement(ContentModel $contentModel, string $buffer, $element): string
    {
        //wenn es keine tl_content-Tabelle ist nicht weiter, da diese von 'modifyForPreview' verarbeitet werden
        if ('tl_content' === $contentModel->rms_ref_table) {
            $buffer = $this->previewContentElement($contentModel, $buffer, $element);
        }

        return $buffer;
    }

    /**
     * Frontend-Preview from not released Content if set get-Parameter 'do=preview'
     * HOOK: getContentElement.
     *
     * ToDo: rewrite this method and create a Hook for other modules
     *
     * @var object
     * @var string
     *
     * @return string
     */
    public function previewContentElement(ContentModel $objElement, string $strBuffer, $element)
    {
        $RmsHelper = RmsHelper::getInstance();
        $rmsSettings = $RmsHelper->getSettings();

        // return if this ignored field
        $ignoreTypedArr = array_map(
            'trim',
            explode(',', $rmsSettings['ignore_content_types'])
        );

        if (\is_array($ignoreTypedArr) && \in_array($objElement->type, $ignoreTypedArr, true)) {
            return $strBuffer;
        }

        if ('preview' === Input::get('do')) {
            $objStoredData = RmsModel::findRef('tl_content', $objElement->id);

            if (null !== $objStoredData) {
                $objRow = $RmsHelper->overwriteDbObj(
                    $objElement,
                    StringUtil::deserialize($objStoredData->data)
                );

                $objRow->published = 1; // news,newsletter
                $objRow->invisible = 0; // content

                $strClass = ContentElement::findClass($objRow->type);

                if (!class_exists($strClass)) {
                    $text = 'Content element class "%s" (content element "%s") does not exist';
                    $logger = Controller::getContainer()->get('monolog.logger.contao');
                    $logger->log(
                        LogLevel::ERROR,
                        sprintf($text, $strClass, $objRow->type),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );

                    return '';
                }

                /** @var ContentElement $objElement */
                $objElement = new $strClass($objRow);
                $strBuffer = $objElement->generate();
            }
        } else {
            if ($objElement->rms_first_save) {
                $strBuffer = '';
            }
        }

        return  $strBuffer;
    }
}
