<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\BackendTemplate;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ContaoRmsBundle\Versions\RmsVersions;

class RmsLog extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * show diff between preview and active version.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function showDiff($row, $href, $label, $title, $icon, $attributes)
    {

        if ('show_diff' === Input::get('key') && $row['ref_id'] === Input::get('ref_id')) {
            $arrFrom = StringUtil::deserialize($row['data_old']);
            $arrTo = StringUtil::deserialize($row['data_new']);

            $objVersions = new RmsVersions($row['ref_table']);
            $objVersions->compare($arrFrom, $arrTo);
        }

        $objTemplate = new BackendTemplate('be_list_button');
        $objTemplate->url = $this->addToUrl($href.'&ref_id='.$row['ref_id']);
        $objTemplate->title = StringUtil::specialchars($title);
        $objTemplate->attributes = $attributes;
        $objTemplate->image = Image::getHtml($icon, $label);

        return  $objTemplate->parse();
    }
}
