<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\ContentModel;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\Versions;
use Psr\Log\LogLevel;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

class Content extends Backend
{
    /**
     * @var RmsHelper
     */
    private $RmsHelper;

    /**
     * @var object|null
     */
    private $logger;

    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('Database');
        $this->import('BackendUser', 'User');
        $this->RmsHelper = RmsHelper::getInstance();
        $this->logger = System::getContainer()->get('monolog.logger.contao');
    }

    /**
     * Return the "toggle send-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        //test rms

        if (Input::get('tid')) {
            $this->toggleVisibility(Input::get('tid'), (1 === Input::get('state')));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_content::invisible', 'alexf')) {
            return '';
        }

        if ($this->RmsHelper->isMemberOfSlaves()) {
            return '';
        }

        $href .= '&amp;id='.Input::get('id').'&amp;tid='.$row['id'].'&amp;state='.$row['invisible'];

        if ($row['invisible']) {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Toggle the visibility of an element.
     *
     * @param int
     * @param bool
     */
    public function toggleVisibility($intId, $blnVisible): void
    {
        // Check permissions to edit
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        // Check permissions to publish
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_content::invisible', 'alexf')) {
            $this->logger->log(
                LogLevel::ERROR,
                'Not enough permissions to show/hide content element ID "'.$intId.'"',
                ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
            );

            $this->redirect('contao/main.php?act=error');
        }

        $objVersions = new Versions('tl_content', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_content']['fields']['invisible']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_content']['fields']['invisible']['save_callback'] as $callback) {
                $this->import($callback[0]);
                $blnVisible = $this->$callback[0]->$callback[1]($blnVisible, $this);
            }
        }

        // Update the database
        $objContent = ContentModel::findByPk($intId);
        $objContent->tstamp = time();
        $objContent->invisible = ($blnVisible ? '' : 1);
        $objContent->save();

        $objVersions->create();
        $this->logger->log(
            LogLevel::INFO,
            'A new version of record "tl_content.id='.$intId.'" has been created',
            ['contao' => new ContaoContext(__METHOD__, 'GENERAL')]
        );
    }

    /**
     * Return the "toggle preview-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function checkPreviewIcon($row, $href, $label, $title, $icon, $attributes)
    {
        $previewLink = $this->RmsHelper->getPreviewLink($row['id'], 'tl_content');

        //test rms
        $objRms = RmsModel::findRef('tl_content', $row['id']);
        if (null === $objRms) {
            return'';
        }

        return '<a href="'.$previewLink.'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
                .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Add the type of content element.
     *
     * @param array
     *
     * @return string
     */
    public function addCteType($arrRow)
    {
        $key = $arrRow['invisible'] ? 'unpublished' : 'published';
        $type = $GLOBALS['TL_LANG']['CTE'][$arrRow['type']][0] ?: '&nbsp;';
        $class = 'limit_height';

        // Remove the class if it is a wrapper element
        if (\in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['start'], true)
            || \in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['separator'], true)
            || \in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['stop'], true)) {
            $class = '';

            if (null !== ($group = $this->getContentElementGroup($arrRow['type']))) {
                $type = $GLOBALS['TL_LANG']['CTE'][$group].' ('.$type.')';
            }
        } elseif (\in_array($arrRow['type'], $GLOBALS['TL_WRAPPERS']['single'], true)) {
            // Add the group name if it is a single element (see #5814)
            if (null !== ($group = $this->getContentElementGroup($arrRow['type']))) {
                $type = $GLOBALS['TL_LANG']['CTE'][$group].' ('.$type.')';
            }
        }

        // Add the ID of the aliased element
        if ('alias' === $arrRow['type']) {
            $type .= ' ID '.$arrRow['cteAlias'];
        }

        // Add the protection status
        if ($arrRow['protected']) {
            $type .= ' ('.$GLOBALS['TL_LANG']['MSC']['protected'].')';
        } elseif ($arrRow['guests']) {
            $type .= ' ('.$GLOBALS['TL_LANG']['MSC']['guests'].')';
        } elseif ($arrRow['rms_new_edit']) {
            $type .= '<span style="color:red;"> ('.$GLOBALS['TL_LANG']['MSC']['rms_new_edit'][0].')</span>';
        }
        // Add the headline level (see #5858)
        if ('headline' === $arrRow['type']) {
            if (\is_array(($headline = StringUtil::deserialize($arrRow['headline'])))) {
                $type .= ' ('.$headline['unit'].')';
            }
        }

        // Limit the element's height
        if (!$GLOBALS['TL_CONFIG']['doNotCollapse']) {
            $class .= ' h64';
        }

        return '
        <div class="cte_type '.$key.'">'.$type.'</div>
        <div class="'.trim($class).'">'.$this->getContentElement($arrRow['id']).'</div>'."\n";
    }

    /**
     * add RMS-Fields in many content-elements (DCA).
     *
     * @var object
     */
    public function addRmsFields(DataContainer $dc): void
    {
        $strTable = Input::get('table');

        //defined blacklist palettes
        $rm_palettes_blacklist = ['__selector__'];

        //add Field in many content-elements
        foreach ($GLOBALS['TL_DCA'][$strTable]['palettes'] as $name => $field) {
            if (\in_array($name, $rm_palettes_blacklist, true)) {
                continue;
            }

            $GLOBALS['TL_DCA'][$strTable]['palettes'][$name] .= ';{rms_legend:hide},rms_notice,rms_release_info';
        }
    }

    /**
     * Return the group of a content element.
     *
     * @param string
     *
     * @return string
     */
    public function getContentElementGroup($element)
    {
        foreach ($GLOBALS['TL_CTE'] as $k => $v) {
            foreach (array_keys($v) as $kk) {
                if ($kk === $element) {
                    return $k;
                }
            }
        }

        return null;
    }
}
