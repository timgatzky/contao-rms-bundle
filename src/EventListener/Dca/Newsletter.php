<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

class Newsletter extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Return the "toggle send-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function checkSendIcon($row, $href, $label, $title, $icon, $attributes)
    {
        //test rms
        $objRms = RmsModel::findRef('tl_newsletter', $row['id']);
        if (null === $objRms) {
            return'';
        }

        return sprintf(
            '<a href="%s" title="%s" %s>%s</a> ',
            $this->addToUrl('id='.$row['id'].'&'.$href),
            StringUtil::specialchars($title),
            $attributes,
            Image::getHtml($icon, $label)
        );
    }

    /**
     * Return the "toggle preview-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function checkPreviewIcon($row, $href, $label, $title, $icon, $attributes)
    {
        //test rms
        $objRms = RmsModel::findRef('tl_newsletter', $row['id']);
        if (null === $objRms) {
            return'';
        }

        $previewLink = RmsHelper::getInstance()->getPreviewLink($row['id'], 'tl_newsletter');

        return sprintf(
            '<a href="%s" title="%s" %s>%s</a> ',
            $previewLink,
            StringUtil::specialchars($title),
            $attributes,
            Image::getHtml($icon, $label)
        );
    }

    /**
     * add RMS-Fields in menny content-elements (DCA).
     *
     * @var object
     */
    public function addRmsFields(DataContainer $dc): void
    {
        $strTable = Input::get('table');

        //defined blacklist palettes
        $rm_palettes_blacklist = ['__selector__'];

        //add Field in meny content-elements
        foreach ($GLOBALS['TL_DCA'][$strTable]['palettes'] as $name => $field) {
            if (\in_array($name, $rm_palettes_blacklist, true)) {
                continue;
            }

            $GLOBALS['TL_DCA'][$strTable]['palettes'][$name] .= ';{rms_legend:hide},rms_notice,rms_release_info';
        }
    }
}
