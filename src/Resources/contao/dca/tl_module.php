<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Add palettes to tl_module.
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['newsreaderrms']
    = $GLOBALS['TL_DCA']['tl_module']['palettes']['newsreader'];

$GLOBALS['TL_DCA']['tl_module']['palettes']['eventreaderrms']
    = $GLOBALS['TL_DCA']['tl_module']['palettes']['eventreader'];

$GLOBALS['TL_DCA']['tl_module']['palettes']['newsletterreaderrms']
    = $GLOBALS['TL_DCA']['tl_module']['palettes']['newsletterreader'];
