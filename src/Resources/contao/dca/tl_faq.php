<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_faq.
 */

if (\Contao\Config::get('rms_active')) {
    \Contao\Controller::loadLanguageFile('tl_default');

    /*
     * change dca from tl_faq
     */
    $GLOBALS['TL_DCA']['tl_faq']['config']['onload_callback'][] = [
        'srhinow.contao_rms_bundle.listener.dca.faq',
        'addRmsFields',
    ];
}

/*
* Fields
*/
$GLOBALS['TL_DCA']['tl_faq']['fields']['ptable']['ignoreDiff'] = true;

$GLOBALS['TL_DCA']['tl_faq']['fields']['rms_first_save'] = [
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_faq']['fields']['rms_new_edit'] = [
    'sql' => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_faq']['fields']['rms_ref_table'] = [
    'sql' => "char(55) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_faq']['fields']['rms_notice'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_notice'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['mandatory' => false, 'rte' => false],
    'sql' => 'longtext NULL',
];

$GLOBALS['TL_DCA']['tl_faq']['fields']['rms_release_info'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_release_info'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
    'save_callback' => [
        ['srhinow.contao_rms_bundle.helper.rms_helper', 'sendEmailInfo'],
    ],
];
