<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_faq_category.
 */
if (\Contao\Config::get('rms_active')) {
    \Contao\Controller::loadLanguageFile('tl_default');

    /*
     * change dca from tl_faq_category
     */
    $GLOBALS['TL_DCA']['tl_faq_category']['config']['onload_callback'][] = [
        'srhinow.contao_rms_bundle.listener.dca.faq_category',
        'addRmsFields',
    ];
    $GLOBALS['TL_DCA']['tl_faq_category']['list']['operations']['editheader']['href']
        = 'act=edit&table=tl_faq_category';

    // Palettes
    $GLOBALS['TL_DCA']['tl_faq_category']['palettes']['__selector__'][] = 'rms_protected';

    // Subpalettes
    $GLOBALS['TL_DCA']['tl_faq_category']['subpalettes']['rms_protected'] = 'rms_master_member,rms_preview_jumpTo';
}

// Fields
$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_protected'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_protected'],
    'exclude' => true,
    'filter' => true,
    'inputType' => 'checkbox',
    'eval' => ['submitOnChange' => true],
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_master_member'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_master_member'],
    'exclude' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_user.name',
    'eval' => ['mandatory' => true, 'chosen' => true],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_preview_jumpTo'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_preview_jumpTo'],
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['mandatory' => false, 'fieldType' => 'radio'],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'eager'],
];
$GLOBALS['TL_DCA']['tl_faq_category']['fields']['ptable']['ignoreDiff'] = true;

$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_first_save'] = [
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_new_edit'] = [
    'sql' => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_ref_table'] = [
    'sql' => "char(55) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_notice'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_notice'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['mandatory' => false, 'rte' => false],
    'sql' => 'longtext NULL',
];

$GLOBALS['TL_DCA']['tl_faq_category']['fields']['rms_release_info'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_release_info'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
    'save_callback' => [
        ['srhinow.contao_rms_bundle.helper.rms_helper', 'sendEmailInfo'],
    ],
];
