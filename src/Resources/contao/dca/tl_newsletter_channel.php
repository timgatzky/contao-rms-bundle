<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_newsletter_channel.
 */
if (\Contao\Config::get('rms_active')) {
    \Contao\Controller::loadLanguageFile('tl_default');

    $GLOBALS['TL_DCA']['tl_newsletter_channel']['config']['onload_callback'][] = [
        'srhinow.contao_rms_bundle.listener.dca.newsletter_channel',
        'addRmsFields',
    ];
    $GLOBALS['TL_DCA']['tl_newsletter_channel']['list']['operations']['editheader']['href']
        = 'act=edit&table=tl_newsletter_channel';

    // Palettes
    $GLOBALS['TL_DCA']['tl_newsletter_channel']['palettes']['__selector__'][] = 'rms_protected';

    // Subpalettes
    $GLOBALS['TL_DCA']['tl_newsletter_channel']['subpalettes']['rms_protected']
        = 'rms_master_member,rms_preview_jumpTo';
}

// Fields
$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_protected'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_protected'],
    'exclude' => true,
    'filter' => true,
    'inputType' => 'checkbox',
    'eval' => ['submitOnChange' => true],
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_master_member'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_master_member'],
    'exclude' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_user.name',
    'eval' => ['mandatory' => true, 'chosen' => true],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_preview_jumpTo'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_preview_jumpTo'],
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['mandatory' => false, 'fieldType' => 'radio'],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'eager'],
];
$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['ptable']['ignoreDiff'] = true;

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_first_save'] = [
    'sql' => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_new_edit'] = [
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_ref_table'] = [
    'sql' => "char(55) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_notice'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_notice'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['mandatory' => false, 'rte' => false],
    'sql' => 'longtext NULL',
];

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['rms_release_info'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_release_info'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
    'save_callback' => [
        ['srhinow.contao_rms_bundle.helper.rms_helper', 'sendEmailInfo'],
    ],
];
