<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Back end modules.
 */
$GLOBALS['TL_LANG']['MOD']['rms'] = ['Release requests', 'This module allows you to release new or changed content.'];
