<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
 $GLOBALS['TL_LANG']['tl_settings']['rms_active'] = [
     'Enable release mode',
     'Only when the select-field is set, the release management is active.',
 ];

/*
 * Legends
 */
 $GLOBALS['TL_LANG']['tl_settings']['rms_legend'] = 'Release Management System (rms)';
