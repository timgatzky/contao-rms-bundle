<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['MSC']['rms_new_edit'] = [
    'Änderungen ohne Freigabe',
    'Dieser Inhalt wurde zwar geändert aber noch nicht freigegeben.',
];

$GLOBALS['TL_LANG']['MSC']['rms_notice'] = [
    'Freigabe-Anmerkung',
    'Die Anmerkung dient dem Verantwortlichen als Hilfestellung der Änderungen.',
];

$GLOBALS['TL_LANG']['MSC']['rms_release_info'] = [
    'Änderungsbescheid senden',
    'Wenn dieses Kästchen ausgewählt wurde, wird der Verantworltiche eine Nachricht per E-Mail erhalten.',
];

$GLOBALS['TL_LANG']['MSC']['rms_release_ok'] = [
    'diese Version freigeben',
    'Wenn dieses Häkchen gesetzt wird, wird diese Version auf der Website freigegeben.',
];

$GLOBALS['TL_LANG']['MSC']['show_preview'] = [
    'Vorschau anzeigen',
    'zeigt die Detailseite mit allen unbestätigten Änderungen an.',
];

$GLOBALS['TL_LANG']['MSC']['rms_protected'] = [
    'Diesen Bereich in die Freigabe-Verwaltung aufnehmen',
    'Die Inhalte in diesem Bereich werden von dem Modul (rms) berücksichtigt.',
];

$GLOBALS['TL_LANG']['MSC']['rms_master_member'] = [
    'verantwortlich für Freigaben',
    'Geben Sie hier die Person an die für die Freigaben in dem Bereich verantwortlich sind.',
];

$GLOBALS['TL_LANG']['MSC']['rms_preview_jumpTo'] = [
    'Freigabe-Vorschauseite',
    'Wenn dieses Feld leer bleibt, wird die "Weiterleitungsseite" als Freigabe-Vorschauseite genommen. 
    Diese kann hier aber überschrieben werden.',
];

/*
 * Messages
 */
$GLOBALS['TL_LANG']['MSC']['rms_succesfully_live'] = 'Die Änderung %s ist jetzt bestätigt und auf der Website zusehen.';

/*
 * E-Mail - Texte
 */
$GLOBALS['TL_LANG']['MSC']['rms_email_subject_question'] = 'Freigabe-Aufforderung';

$GLOBALS['TL_LANG']['MSC']['rms_email_subject_answer'] = 'Freigabe-Aufforderung (Antwort)';

$GLOBALS['TL_LANG']['MSC']['rms_email_intro'] = [
    'Sehr geehrte Damen und Herren,',
    'ein Element wurde erzeugt / ein bestehendes Element bearbeitet.',
];

$GLOBALS['TL_LANG']['MSC']['rms_email_instruction'] = 'Bitte prüfen Sie die Änderungen und geben Sie diese frei 
oder lehnen sie ab.';

$GLOBALS['TL_LANG']['MSC']['rms_email_closing'] = 'Mit freundlichen Grüßen';
