<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_settings']['rms_active'] = [
    'Freigabe-Modus aktivieren',
    'Erst wenn der Haken gesetzt ist, ist die Freigabe-Verwaltung aktiv.',
];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_settings']['rms_legend'] = 'Freigabe-Modul';
