<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Legends.
 */
$GLOBALS['TL_LANG']['tl_calendar_events']['rms_legend'] = 'Freigabe-Benachrichtigung';
