<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * global Operation.
 */
$GLOBALS['TL_LANG']['tl_rms']['show_preview'] = [
    'Export',
    'Rechnungen und deren Posten in CSV-Dateien exportieren.',
];

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_rms']['tstamp'] = ['Freigabe-Datum', 'Datum der Freigabe-Anfrage.'];
$GLOBALS['TL_LANG']['tl_rms']['ref_table'] = ['geänderte Tabelle', 'referenzierte Tabelle der Änderung.'];
$GLOBALS['TL_LANG']['tl_rms']['ref_author'] = ['Autor der Änderung', ''];
$GLOBALS['TL_LANG']['tl_rms']['ref_id'] = ['ID', 'Eindeutige Datenbank-ID des geänderten Datensatzes'];
$GLOBALS['TL_LANG']['tl_rms']['ref_notice'] = [
    'Freigabe-Anmerkung',
    'Die Anmerkung dient dem Verantwortlichen als Hilfestellung der Änderungen.',
];
$GLOBALS['TL_LANG']['tl_rms']['edit_url'] = ['Pfad zur Bearbeitungsansicht', ''];
$GLOBALS['TL_LANG']['tl_rms']['data'] = ['geänderte Daten', 'serialisierte Daten des Datensatzes'];
$GLOBALS['TL_LANG']['tl_rms']['status'] = ['Status', 'zeigt an ob diese Freigabe bereits beantwortet wurde.'];
$GLOBALS['TL_LANG']['tl_rms']['region'] = ['Bereich'];
$GLOBALS['TL_LANG']['tl_rms']['preview_link'] = ['Vorschau-Link'];
$GLOBALS['TL_LANG']['tl_rms']['last_edit'] = ['letzte Bearbeitung'];
$GLOBALS['TL_LANG']['tl_rms']['status_options'] = [
    '0' => 'Zur Freigabe',
    '1' => 'zurückgegeben',
    '2' => 'In Bearbeitung',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_rms']['settings'] = ['Freigabe-Einstellungen', ''];
$GLOBALS['TL_LANG']['tl_rms']['logs'] = ['Freigabe-Logs', ''];
$GLOBALS['TL_LANG']['tl_rms']['acknowledge'] = ['Änderung freigeben', 'diese Änderung freigeben.'];
$GLOBALS['TL_LANG']['tl_rms']['delete'] = ['Änderung löschen', 'diese Änderung löschen.'];
$GLOBALS['TL_LANG']['tl_rms']['edit'] = ['Inhalt bearbeiten', 'Inhalt bearbeiten'];
$GLOBALS['TL_LANG']['tl_rms']['show_diff'] = ['Unterschied anzeigen', 'Den bearbeiteten Unterschied anzeigen.'];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_content']['rm_legend'] = 'Freigabe';

/*
* Bereiche
*/
$GLOBALS['TL_LANG']['tl_rms']['sessions']['article_tl_article'] = 'Artikel';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['article_tl_content'] = 'Artikel :: Inhaltselement';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['newsletter_tl_newsletter'] = 'Newsletter :: Element';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['newsletter_tl_content'] = 'Newsletter :: Inhaltselement';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['calendar_tl_content'] = 'Kalender-Event :: Inhaltselement';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['calendar_tl_calendar_events'] = 'Kalender :: Event';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['news_tl_news'] = 'Nachrichten :: Beitrag';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['news_tl_content'] = 'Nachrichten :: Inhaltselement';

/*
* anderer Text
*/
$GLOBALS['TL_LANG']['tl_rms']['diff_new_content'] = '<h5>Der Inhalt wurde neu erstellt.</h5> 
<p>Es gibt daher keine Version zum Vergleich.</p>';
$GLOBALS['TL_LANG']['tl_rms']['info_new_edit'] = '*neu erstellt*';
$GLOBALS['TL_LANG']['tl_rms']['list_empty_notice'] = 'keine';
$GLOBALS['TL_LANG']['tl_rms']['list_email_title'] = 'E-Mail schreiben.';
$GLOBALS['TL_LANG']['tl_rms']['list_preview_title'] = 'Die Änderung als Preview auf der Website anzeigen.';
