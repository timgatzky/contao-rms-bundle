<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * -------------------------------------------------------------------------
 * RMS-SETTINGS
 * -------------------------------------------------------------------------
 */
$GLOBALS['RMS']['SETTINGS_ID'] = 1;
$GLOBALS['RMS_PUBLIC_FOLDER'] = 'bundles/srhinowcontaorms';

if (\Contao\Config::get('rms_active')) {
    array_insert($GLOBALS['BE_MOD']['content'], 1, [
        'rms' => [
            'tables' => ['tl_rms', 'tl_rms_settings', 'tl_rms_log'],
            'icon' => $GLOBALS['RMS_PUBLIC_FOLDER'].'/icons/promotion.png',
            'stylesheet' => $GLOBALS['RMS_PUBLIC_FOLDER'].'/css/be.css',
            'acknowledge' => ['srhinow.contao_rms_bundle.listener.dca.rms', 'acknowledgeEntry'],
        ],
    ]);

    // article
    $GLOBALS['BE_MOD']['content']['article']['showPreview'] = [
        'Srhinow\ContaoRmsBundle\Helper\RmsHelper',
        'showPreviewInBrowser',
    ];
    $GLOBALS['BE_MOD']['content']['article']['stylesheet'] = $GLOBALS['RMS_PUBLIC_FOLDER'].'/css/be.css';

    // news
    $GLOBALS['BE_MOD']['content']['news']['showPreview'] = [
        'Srhinow\ContaoRmsBundle\Helper\RmsHelper',
        'showPreviewInBrowser',
    ];
    $GLOBALS['BE_MOD']['content']['news']['stylesheet'] = $GLOBALS['RMS_PUBLIC_FOLDER'].'/css/be.css';

    // calendar
    $GLOBALS['BE_MOD']['content']['calendar']['showPreview'] = [
        'Srhinow\ContaoRmsBundle\Helper\RmsHelper',
        'showPreviewInBrowser',
    ];
    $GLOBALS['BE_MOD']['content']['calendar']['stylesheet'] = $GLOBALS['RMS_PUBLIC_FOLDER'].'/css/be.css';

    // newsletter
    $GLOBALS['BE_MOD']['content']['newsletter']['showPreview'] = [
        'Srhinow\ContaoRmsBundle\Helper\RmsHelper',
        'showPreviewInBrowser',
    ];
    $GLOBALS['BE_MOD']['content']['newsletter']['stylesheet'] = $GLOBALS['RMS_PUBLIC_FOLDER'].'/css/be_nl.css';

    // HOOKs
    $GLOBALS['TL_HOOKS']['loadDataContainer'][] = [
        'srhinow.contao_rms_bundle.listener.hook.load_data_container',
        'onLoadDataContainer',
    ];
    $GLOBALS['TL_HOOKS']['getContentElement'][] = [
        'srhinow.contao_rms_bundle.listener.hook.get_content_element',
        'onGetContentElement',
    ];
    $GLOBALS['TL_HOOKS']['parseTemplate'][] = [
        'srhinow.contao_rms_bundle.listener.hook.parse_template',
        'onParseTemplate',
    ];
    $GLOBALS['TL_HOOKS']['executePostActions'][] = [
        'srhinow.contao_rms_bundle.listener.hook.execute_post_actions',
        'onExecutePostActions',
    ];

    // um die bearbeiteten Elementgenerator-Felder zu laden
//    $GLOBALS['TL_HOOKS']['rewriteDcaFields'][] = ['Srhinow\ContaoRmsBundle\Helper\RmsHelper', 'rewriteDcaFields'];
}

/*
 * -------------------------------------------------------------------------
 * MODELS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_rms'] = \Srhinow\ContaoRmsBundle\Model\RmsModel::class;
$GLOBALS['TL_MODELS']['tl_rms_settings'] = \Srhinow\ContaoRmsBundle\Model\RmsSettingsModel::class;
$GLOBALS['TL_MODELS']['tl_rms_tmp'] = \Srhinow\ContaoRmsBundle\Model\RmsTmpModel::class;
$GLOBALS['TL_MODELS']['tl_rms_log'] = \Srhinow\ContaoRmsBundle\Model\RmsLogModel::class;

/*
 * -------------------------------------------------------------------------
 * FRONTEND-MODULE
 * -------------------------------------------------------------------------
 */
$GLOBALS['FE_MOD']['news']['newsreaderrms'] = 'Srhinow\ContaoRmsBundle\Modules\ModuleNewsReaderRms';
$GLOBALS['FE_MOD']['newsletter']['newsletterreaderrms'] = 'Srhinow\ContaoRmsBundle\Modules\ModuleNewsletterReaderRms';
$GLOBALS['FE_MOD']['events']['eventreaderrms'] = 'Srhinow\ContaoRmsBundle\Modules\ModuleEventReaderRms';

/*
 * -------------------------------------------------------------------------
* source tables that have rms enabled
 * -------------------------------------------------------------------------
 */
$GLOBALS['rms_extension']['tables'][] = 'tl_article';
$GLOBALS['rms_extension']['tables'][] = 'tl_content';
$GLOBALS['rms_extension']['tables'][] = 'tl_newsletter';
$GLOBALS['rms_extension']['tables'][] = 'tl_newsletter_channel';
$GLOBALS['rms_extension']['tables'][] = 'tl_faq';
$GLOBALS['rms_extension']['tables'][] = 'tl_faq_category';
$GLOBALS['rms_extension']['tables'][] = 'tl_news';
$GLOBALS['rms_extension']['tables'][] = 'tl_news_archive';
$GLOBALS['rms_extension']['tables'][] = 'tl_calendar_events';
$GLOBALS['rms_extension']['tables'][] = 'tl_calendar';

/*
 * -------------------------------------------------------------------------
* einige Felder wie Spaltensets können ignoriert werden
 * -------------------------------------------------------------------------
 */
$GLOBALS['rms_extension']['ignoredFields'] = ['colsetStart', 'colsetPart', 'colsetEnd'];
