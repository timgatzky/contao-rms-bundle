<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Modules;

use Contao\BackendTemplate;
use Contao\Comments;
use Contao\CoreBundle\Exception\InternalServerErrorException;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\Environment;
use Contao\Input;
use Contao\ModuleNewsReader;
use Contao\NewsArchiveModel;
use Contao\NewsModel;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Contao\UserModel;
use Patchwork\Utf8;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

/**
 * Front end module "news reader".
 *
 * @property Comments $Comments
 * @property string   $com_template
 * @property array    $news_archives
 */
class ModuleNewsReaderRms extends ModuleNewsReader
{
    /**
     * Display a wildcard in the back end.
     *
     * @throws InternalServerErrorException
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### '
                .Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['newsreader'][0])
                .' - RMS-Preview ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        /* @var PageModel $objPage */
        global $objPage;

        $this->Template->articles = '';
        $this->Template->referer = 'javascript:history.go(-1)';
        $this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];

        // Get the news item
        $objArticle = NewsModel::findPublishedByParentAndIdOrAlias(Input::get('items'), $this->news_archives);

        // The news item does not exist or has an external target (see #33)
        if (null === $objArticle || 'default' !== $objArticle->source) {
            throw new PageNotFoundException('Page not found: '.Environment::get('uri'));
        }

        if ('preview' === Input::get('do')) {
            $objStoredData = RmsModel::findRef('tl_news', $objArticle->id);

            if (null !== $objStoredData) {
                $RmsHelper = RmsHelper::getInstance();
                $objArticle = $RmsHelper->overwriteDbObj(
                    $objArticle,
                    StringUtil::deserialize($objStoredData->data)
                );
            }
        }

        // Set the default template
        if (!$this->news_template) {
            $this->news_template = 'news_full';
        }

        $arrArticle = $this->parseArticle($objArticle);
        $this->Template->articles = $arrArticle;

        // Overwrite the page title (see #2853, #4955 and #87)
        if ($objArticle->pageTitle) {
            $objPage->pageTitle = $objArticle->pageTitle;
        } elseif ($objArticle->headline) {
            $objPage->pageTitle = strip_tags(StringUtil::stripInsertTags($objArticle->headline));
        }

        // Overwrite the page description
        if ($objArticle->description) {
            $objPage->description = $objArticle->description;
        } elseif ($objArticle->teaser) {
            $objPage->description = $this->prepareMetaDescription($objArticle->teaser);
        }

        $bundles = System::getContainer()->getParameter('kernel.bundles');

        // HOOK: comments extension required
        if ($objArticle->noComments || !isset($bundles['ContaoCommentsBundle'])) {
            $this->Template->allowComments = false;

            return;
        }

        /** @var NewsArchiveModel $objArchive */
        $objArchive = $objArticle->getRelated('pid');
        $this->Template->allowComments = $objArchive->allowComments;

        // Comments are not allowed
        if (!$objArchive->allowComments) {
            return;
        }

        // Adjust the comments headline level
        $intHl = min((int) str_replace('h', '', $this->hl), 5);
        $this->Template->hlc = 'h'.($intHl + 1);

        $this->import(Comments::class, 'Comments');
        $arrNotifies = [];

        // Notify the system administrator
        if ('notify_author' !== $objArchive->notify) {
            $arrNotifies[] = $GLOBALS['TL_ADMIN_EMAIL'];
        }

        /** @var UserModel $objAuthor */
        if ('notify_admin' !== $objArchive->notify
            && (
                $objAuthor = $objArticle->getRelated('author')) instanceof UserModel && '' !== $objAuthor->email
        ) {
            $arrNotifies[] = $objAuthor->email;
        }

        $objConfig = new \stdClass();

        $objConfig->perPage = $objArchive->perPage;
        $objConfig->order = $objArchive->sortOrder;
        $objConfig->template = $this->com_template;
        $objConfig->requireLogin = $objArchive->requireLogin;
        $objConfig->disableCaptcha = $objArchive->disableCaptcha;
        $objConfig->bbcode = $objArchive->bbcode;
        $objConfig->moderate = $objArchive->moderate;

        $this->Comments->addCommentsToTemplate($this->Template, $objConfig, 'tl_news', $objArticle->id, $arrNotifies);
    }
}
