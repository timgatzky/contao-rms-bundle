<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/**
 * set namespace.
 */

namespace Srhinow\ContaoRmsBundle\Versions;

use Contao\Backend;
use Contao\Config;
use Contao\Controller;
use Contao\CoreBundle\Exception\ResponseException;
use Contao\Environment;
use Contao\StringUtil;
use Contao\System;
use Contao\Validator;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;

/**
 * Class RmsVersions (fork from Contao-Core-Versions-Class).
 */
class RmsVersions extends Controller
{
    /**
     * Table.
     *
     * @var array
     */
    protected $refTable = '';

    protected $firstSave = true;

    /**
     * Initialize the object.
     *
     * @param string
     * @param int
     */
    public function __construct($refTable)
    {
        parent::__construct();
        $this->refTable = $refTable;
    }

    /**
     * Compare versions.
     */
    public function compare($arrFrom, $arrTo): void
    {
        $strBuffer = '';
        $firstSave = false;

        $settings = RmsHelper::defineSettings();

        // Only continue if both version numbers are set

        System::loadLanguageFile($this->refTable);
        $this->loadDataContainer($this->refTable);

        $arrOrder = [];

        $arrFields = $GLOBALS['TL_DCA'][$this->refTable]['fields'];

        // if first save show text rather then different list
        $this->firstSave = (1 === $arrFrom['rms_first_save']) ? true : false;

        // Get the order fields
        foreach ($arrFields as $i => $arrField) {
            if (isset($arrField['eval']['orderField'])) {
                $arrOrder[] = $arrField['eval']['orderField'];
            }
        }
//        foreach ($arrTo as $k => $v) {
//            dump($arrFrom[$k]);
//            dump($arrTo[$k]);
//
//        }
//        die();
        // Find the changed fields and highlight the changes
        foreach ($arrTo as $k => $v) {
            if ($arrFrom[$k] !== $arrTo[$k]) {
                if ('password' === $arrFields[$k]['inputType']
                        || $arrFields[$k]['eval']['doNotShow']
                        || $arrFields[$k]['eval']['hideInput']
                        || true === $arrFields[$k]['ignoreDiff']) {
                    continue;
                }

                // weitere in der Anzeige, zuignorierende Felder aus den rms-Einstellungen prüfen
                $ignoreFieldArr = array_map('trim', explode(',', $settings['ignore_fields']));
                if (\is_array($ignoreFieldArr) && \in_array($k, $ignoreFieldArr, true)) {
                    continue;
                }

                $blnIsBinary = ('fileTree' === $arrFields[$k]['inputType'] || \in_array($k, $arrOrder, true));

                // Convert serialized arrays into strings
                if (\is_array(($tmp = StringUtil::deserialize($arrFrom[$k]))) && !\is_array($arrFrom[$k])) {
                    $arrFrom[$k] = $this->implodeRecursive($tmp, $blnIsBinary);
                }
                if (\is_array(($tmp = StringUtil::deserialize($arrTo[$k]))) && !\is_array($arrTo[$k])) {
                    $arrTo[$k] = $this->implodeRecursive($tmp, $blnIsBinary);
                }
                unset($tmp);

                // Convert already deserialized arrays to strings - TODO Why are they already deserialzed?
                if (\is_array($arrFrom[$k])) {
                    $arrFrom[$k] = $this->implodeRecursive($arrFrom[$k], $blnIsBinary);
                }
                if (\is_array($arrTo[$k])) {
                    $arrTo[$k] = $this->implodeRecursive($arrTo[$k], $blnIsBinary);
                }

                // Convert binary UUIDs to their hex equivalents (see #6365)
                if ($blnIsBinary && \Validator::isUuid($arrFrom[$k])) {
                    $arrFrom[$k] = \StringUtil::binToUuid($arrFrom[$k]);
                }
                if ($blnIsBinary && \Validator::isUuid($arrTo[$k])) {
                    $arrTo[$k] = \StringUtil::binToUuid($arrTo[$k]);
                }

                // Convert date fields
                if ('date' === $arrFields[$k]['eval']['rgxp']) {
                    $arrFrom[$k] = \Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $arrFrom[$k] ?: '');
                    $arrTo[$k] = \Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $arrTo[$k] ?: '');
                } elseif ('time' === $arrFields[$k]['eval']['rgxp']) {
                    $arrFrom[$k] = \Date::parse($GLOBALS['TL_CONFIG']['timeFormat'], $arrFrom[$k] ?: '');
                    $arrTo[$k] = \Date::parse($GLOBALS['TL_CONFIG']['timeFormat'], $arrTo[$k] ?: '');
                } elseif ('datim' === $arrFields[$k]['eval']['rgxp'] || 'tstamp' === $k) {
                    $arrFrom[$k] = \Date::parse($GLOBALS['TL_CONFIG']['datimFormat'], $arrFrom[$k] ?: '');
                    $arrTo[$k] = \Date::parse($GLOBALS['TL_CONFIG']['datimFormat'], $arrTo[$k] ?: '');
                }

                // Convert strings into arrays
                if (!\is_array($arrTo[$k])) {
                    $arrTo[$k] = explode("\n", $arrTo[$k]);
                } else {
                    // auf multicolumnfelder testen (value als Array) und dann serialisieren
                    // damit es als string versioniert werden kann
                    foreach ($arrTo[$k] as $tk => $tv) {
                        if (\is_array($tv)) {
                            $arrTo[$k][$tk] = serialize($tv);
                        }
                    }
                }

                if (!\is_array($arrFrom[$k])) {
                    $arrFrom[$k] = (null === $arrFrom[$k])? null : explode("\n", $arrFrom[$k]);
                } else {
                    foreach ($arrFrom[$k] as $fk => $fv) {
                        if (\is_array($fv)) {
                            $arrFrom[$k][$fk] = serialize($fv);
                        }
                    }
                }

                $objDiff = new \Diff($arrFrom[$k], $arrTo[$k]);

                $strBuffer .= $objDiff->render(
                    new \Contao\DiffRenderer(
                        ['field' => ($arrFields[$k]['label'][0] ?: (isset($GLOBALS['TL_LANG']['MSC'][$k])
                            ? (\is_array($GLOBALS['TL_LANG']['MSC'][$k])
                                ? $GLOBALS['TL_LANG']['MSC'][$k][0]
                                : $GLOBALS['TL_LANG']['MSC'][$k])
                            : $k)),
                        ]
                    )
                );
            }
        }

        // Identical versions
        if ('' === $strBuffer) {
            $strBuffer = '<p>'.$GLOBALS['TL_LANG']['MSC']['identicalVersions'].'</p>';
        }

        $this->createOutput($strBuffer);
    }

    public function createOutput($strBuffer = ''): void
    {
        $objTemplate = new \BackendTemplate('be_rmsdiff');

        // Template variables
        $objTemplate->content = $strBuffer;
        $objTemplate->theme = Backend::getTheme();
        $objTemplate->base = Environment::get('base');
        $objTemplate->language = $GLOBALS['TL_LANGUAGE'];
        $objTemplate->charset = $GLOBALS['TL_CONFIG']['characterSet'];
        $objTemplate->action = ampersand(Environment::get('request'));
        $objTemplate->firstSave = $this->firstSave;
        $objTemplate->diffNewContent = $GLOBALS['TL_LANG']['tl_rms']['diff_new_content'];
        $objTemplate->title = StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['showDifferences']);
        $GLOBALS['TL_CONFIG']['debugMode'] = false;
        $objTemplate->charset = Config::get('characterSet');

        throw new ResponseException($objTemplate->getResponse());
    }

    /**
     * Implode a multi-dimensional array recursively.
     *
     * @param bool $binary
     *
     * @return string
     */
    protected function implodeRecursive($var, $binary = false)
    {
        if (!\is_array($var)) {
            return $binary && Validator::isBinaryUuid($var) ? StringUtil::binToUuid($var) : $var;
        }

        if (!\is_array(current($var))) {
            if ($binary) {
                $var = array_map(
                    static function ($v) {
                        return Validator::isBinaryUuid($v) ? StringUtil::binToUuid($v) : $v;
                    },
                    $var
                );
            }

            return implode(', ', $var);
        }

        $buffer = '';

        foreach ($var as $k => $v) {
            $buffer .= $k.': '.$this->implodeRecursive($v)."\n";
        }

        return trim($buffer);
    }
}
